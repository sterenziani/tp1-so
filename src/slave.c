#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "lib/resources.h"

#define SEMAPHORE_IS_OFF 0
#define SEMAPHORE_IS_ON 1

// Lee del fd hasta encontrar un \n. Retorna la cantidad de caracteres leidos hasta ese \n inclusive
// El flag en SEMAPHORE_IS_OFF implica que aun no tengo control del semaforo y por lo tanto necesito tomarlo dentro de la función
int readline(sem_t* semaphore, char* line, int semaphore_flag, int fd)
{
  if(semaphore_flag == SEMAPHORE_IS_OFF)
    sem_wait(semaphore);
  int linelen = 0;
  do
  {
    read(fd, line+linelen, 1);
    linelen++;
  }while(line[linelen-1] != '\n');
  if(semaphore_flag == SEMAPHORE_IS_OFF)
    sem_post(semaphore);
  return linelen;
}

void hash(char* filename, int linelen, sem_t* semaphore, int slave_code)
{
  if(fork() == 0)
  {
    filename[linelen-1] = 0;
    // Hijo activa semáforo
    sem_wait(semaphore);
    if(filename[0] != 0)
    {
      //printf("Slave #%d will now hash the file called %s\n", slave_code, filename);
      fflush(stdout);
      execl("/usr/bin/md5sum", "md5sum", filename, NULL);
    }
  }
  else
  {
    // Padre espera a que vuelva el hijo que se llevó el semáforo y lo vuelve a poner
    wait(0);
    sem_post(semaphore);
  }
}

int main(int args, char* argv[])
{
  sem_t* semaphore;
  semaphore = sem_open(SEM_NAME, O_RDWR);
  int initial_amount = atoi(argv[1]);
  int initial_read_fd = atoi(argv[2]);
  char* line = malloc(PATH_MAX);
  int slave_code = getpid();
  int linelen;

  int i;
  char initial_files[initial_amount][PATH_MAX];
  int initial_files_length[initial_amount];
  // Ya está validado que la cantidad inicial es menor o igual al total de archivos que leerá este esclavo
  // Pauso la lectura del pipe hasta extraer mis archivos y luego la libero
  sem_wait(semaphore);
  for (i = 0; i < initial_amount; i++)
  {
    initial_files_length[i] = readline(semaphore, initial_files[i], SEMAPHORE_IS_ON, initial_read_fd);
  }
  sem_post(semaphore);

  for (i = 0; i < initial_amount; i++)
  {
    hash(initial_files[i], initial_files_length[i], semaphore, slave_code);
  }
  sem_wait(semaphore);
  //printf("Slave #%d is done with their initial workload!\n", slave_code);
  fflush(stdout);
  sem_post(semaphore);

  // Ya leyó los iniciales, ahora podría toparme con un \n que simboliza fin de lista
  // Lee de stdin hasta que lea un \n suelto (que simboliza que ya no hay mas archivos)
	while((linelen = readline(semaphore, line, SEMAPHORE_IS_OFF, 0)) > 0 && *line != '\n')
	{
    hash(line, linelen, semaphore, slave_code);
  }
  free(line);
  sem_unlink(SEM_NAME);
  return 0;
}
