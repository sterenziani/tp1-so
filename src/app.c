#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "lib/pipes.h"
#include "lib/resources.h"
#include "lib/shm.h"

#define MIN_SLAVES 2
#define MAX_SLAVES 10
#define MAX_INITIAL_AMOUNT 10
#define SHMMEM_MODE (mode_t) 0777
#define OUTPUT_FILE "output.txt"

int main(int args, char *argv[])
{
  size_t size = 0; 
  char *pid_string = pid_to_string(getpid(), &size);
  sem_t *sem = sem_open(pid_string, O_CREAT | O_RDWR, DEFAULT_FILE_MODE, SEM_VIEW_INITIAL_VALUE);  
  printf("%d\n", getpid());
  fflush(stdout);
  sleep(10);
  sem_t *semaphore = sem_open(SEM_NAME, O_CREAT, SEM_PERMS, SEM_INITIAL_VALUE);
  
  // Let's create the pipes
  int initial_pipe[2];
  pipe (initial_pipe);
  int pipe_to_slaves[2];
  pipe (pipe_to_slaves);
  int pipe_to_master[2];
  pipe (pipe_to_master);
  int initial_read_fd = initial_pipe[READ_TERMINAL];
  char initial_read_fd_str[3];
  sprintf(initial_read_fd_str, "%d", initial_read_fd);

  // Let's set the amount of slaves and the size of their initial workload
  int amount_of_files = args-1;
  int amount_of_slaves = amount_of_files/100;
  if(amount_of_slaves < MIN_SLAVES)
    amount_of_slaves = MIN_SLAVES;
  if(amount_of_slaves > MAX_SLAVES)
    amount_of_files = MAX_SLAVES;
  int initial_amount_per_slave = (amount_of_files/2)/amount_of_slaves;
  if(initial_amount_per_slave > MAX_INITIAL_AMOUNT)
    initial_amount_per_slave = MAX_INITIAL_AMOUNT;
  int digits_of_max_initial_amount = 0;
  int aux = initial_amount_per_slave;
  while(aux > 0)
  {
    aux = aux/10;
    digits_of_max_initial_amount++;
  }
  char initial_amount_str[digits_of_max_initial_amount];
  sprintf(initial_amount_str, "%d", initial_amount_per_slave);
  //printf("Each slave will process at least %d files each\n", initial_amount_per_slave);

  // Let's create an array with all of our slaves
  pid_t slaves[amount_of_slaves];
  int i=0;
  int fork_result;
  for (i = 0; i < amount_of_slaves; i++)
  {
    fork_result = fork();
    if(fork_result != 0)
    {
      slaves[i] = fork_result;
      //printf("\tCreating child #%d\n", fork_result);
    }
    else
    {
      // This is a child, let's replace its STDIN and STDOUT with ReadToSlaves and WriteToMaster
      // Now that everything is set, let's get this slave to work
      read_on_pipe(pipe_to_slaves);
      write_on_pipe(pipe_to_master);
      close(STDERR_FILENO);
      execl("./slave", "slave", initial_amount_str, initial_read_fd_str, NULL);
    }
  }

  // From this point onwards, only the Master can read this code
  // Master replaces STDIN and STDOUT with ReadToMaster and WriteToSlaves
  int saved_stdout = 1;
  saved_stdout = dup(1);
  write_on_pipe(initial_pipe);
  read_on_pipe(pipe_to_master);


  // Master writes files to hash into the pipe
  // First we send the initial workloads through initial_pipe
  for(i = 1; i <= initial_amount_per_slave*amount_of_slaves; i++){
      puts(argv[i]);
  }

  // All remaining files go through pipe_to_slaves
  write_on_pipe(pipe_to_slaves);
  for (; i < args; i++){
    puts(argv[i]);
  }

  // Let's send one '\n' for each slave, signaling the end of the list
  for(i=0; i < amount_of_slaves; i++){
    puts("\n");
  }

  // Let's restore STDOUT so we can print to console
  dup2(saved_stdout, 1);
  close(saved_stdout);

  // Let's convert our hashed files into the desired format and output to both STDOUT and output.txt
  char* line = NULL;
  char* hash = NULL;
  char* filename = NULL;
	size_t linecap = 0;
	ssize_t linelen;
  const char s[2] = " ";
  const char s2[2] = "\n";
  
  shm_ptr hash_mem;
  hash_mem = new_shm(pid_string, DEFAULT_FILE_MODE, amount_of_files-1, HASH_MAX);

  sem_post(sem);

  FILE *f = fopen(OUTPUT_FILE, "w");
  if (f == NULL)
      exit(1);
  char buff[HASH_MAX];

  while((linelen = getline(&line, &linecap, stdin)) > 0)
  {
    // If it doesn't start with S, it's a hash that we should convert to the desired format and output it
    hash = strtok(line, s);
    filename = strtok(NULL, s);
    filename = strtok(filename, s2);
    sprintf(buff, HASH_FORMAT, filename, hash);
    write_shm(hash_mem, buff);
    sem_post(sem);
    fprintf(f, "%s\n", buff);
  }
  fclose(f);
  free(line);

  // Let's wait for our slaves to finish their work
  for (i = 0; i < amount_of_slaves; i++)
    waitpid(slaves[i], NULL, 0);
  int view_id = get_view_pid(hash_mem);
  if (view_id != 0){
    waitpid(view_id, NULL, 0);
  }
  sem_close(semaphore);
  sem_unlink(SEM_NAME);
  sem_close(sem);
  sem_unlink(pid_string);
  close_pipe(initial_pipe);
  close_pipe(pipe_to_slaves);
  close_pipe(pipe_to_master);
  finalize_shm(hash_mem);
  free(pid_string);


  return 0;
}
