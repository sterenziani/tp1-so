#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <err.h>
#include "lib/shm.h"
#include "lib/resources.h"

#define NO_PID_PROVIDED "An app pid must be provided as parameter\n"
#define INVALID_SEMAPHORE "No app asociated with pid %s\n"
#define INVALID_SHARED_MEMORY "Could not open Shared Memory instace from pid %s\n"

int main(int args, char *argv[])
{
  /*
    First we validate there are two parameters the first one is always the programs name 
    and the second one should be the APP pid
  */
  if (args != 2) {
    printf(NO_PID_PROVIDED);
    return 1;
  }
  shm_ptr hash_mem;
  /*
    We expect APP to have created the semaphore with its pid as its the first thing it does.
    If there is no semaphore then an invalid pid was given
  */
  sem_t *sem = sem_open(argv[1], O_RDWR);
  if (sem == SEM_FAILED)
  {
    printf(INVALID_SEMAPHORE, argv[1]);
    return 1;
  }

  //Before opening a Shared Memory instance, APP must have created it, so we wait for exacly that
  sem_wait(sem);
  hash_mem = open_shm(argv[1], DEFAULT_FILE_MODE);
  if (hash_mem == MAP_FAILED) {
    printf(INVALID_SHARED_MEMORY, argv[1]);
    return 1;
  }

  //Creates a buffer of the maximum size something returned by md5sum could be
  char buff[HASH_MAX];
  int could_read;
  do
  {
    //Coordinates with app to read as it writes in order not to create a Busy Waiting situation
    sem_wait(sem);
    could_read = read_shm(hash_mem, buff);
    //Prints to screen what was in Shared Memory
    printf("%s\n", buff);
  } while(could_read);

  //Frees the structure and releases Shared Memory mappings from this client
  close_shm(hash_mem);
  return 0;
}
