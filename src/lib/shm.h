#ifndef _SHM_H_
#define _SHM_H_

#include <limits.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

typedef struct shm_accessor *shm_ptr;

int write_shm(shm_ptr accesor, const char *text);
int read_shm(shm_ptr accesor, char *text);

/*
	Admin
*/

shm_ptr new_shm(const char *shm_filename, mode_t mode, size_t buffer_size, size_t data_size);
pid_t get_view_pid(shm_ptr accessor);
void finalize_shm(shm_ptr accesor);

/*
	Client
*/

shm_ptr open_shm(const char *shm_filename, mode_t mode);
void close_shm(shm_ptr accessor);

#endif