#ifndef _RESOURCES_H_
#define _RESOURCES_H_

#include <sys/types.h>

#define SEM_NAME "ITBA.TP1.SEMAPHORE"
#define SEM_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define SEM_INITIAL_VALUE 1
#define SEM_VIEW_INITIAL_VALUE 0
#define PID_0 "0"
#define DEFAULT_FILE_MODE (mode_t) 0777

/*
	A file can at max be of PATH_MAX length (on most linux filesystems there is a restriction of 256 bytes but this is not standarized as oposed to PATH_MAX)
	but a file hash is of 32 chars (bytes) plus 3 chars more for the "\t: ".
*/
#define HASH_FORMAT "%s\t: %s"
#define HASH_MAX PATH_MAX+32*sizeof(char)+3*sizeof(char)

/*
	Allocates a string containing a pid transformed into a string
*/
char *pid_to_string(pid_t pid, size_t* size);

#endif

