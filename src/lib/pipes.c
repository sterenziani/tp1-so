/*** Libraries ***/

#include <unistd.h>
#include <stdio.h>
#include "pipes.h"

/*** endLibraries ***/

/*** Functions ***/

void read_on_pipe(pipe_t pipe){
	listener_on_pipe(pipe, READ_TERMINAL, STDIN_FILENO);
}

void write_on_pipe(pipe_t pipe){
	listener_on_pipe(pipe, WRITE_TERMINAL, STDOUT_FILENO);
}

void listener_on_pipe(pipe_t pipe, int terminal, int fd){
	dup2(pipe[terminal], fd);
	close_pipe(pipe);
}

void close_pipe(pipe_t pipe){
	close(pipe[READ_TERMINAL]);
	close(pipe[WRITE_TERMINAL]);
}

/*** endFunctions ***/
