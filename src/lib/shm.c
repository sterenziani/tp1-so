#include "shm.h"
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define SHMMEM_FLAGS O_RDWR | O_CREAT
#define SHMMEM_PROT PROT_READ | PROT_WRITE
#define BUFFER_POINTER(__accessor)  (__accessor->buffer_struct + sizeof(shm_buffer_t))

typedef struct {
	pid_t view;
	size_t counter;
	size_t read_index;
	size_t write_index;
	size_t buffer_size;
	size_t max_text_legth;
} shm_buffer_t;

typedef struct shm_accessor{
	int fd;
	char filename[PATH_MAX];
	size_t fd_size;
	shm_buffer_t *buffer_struct;
} shm_accessor_t;

//PATH_MAX+36

shm_ptr new_shm(const char *shm_filename, mode_t mode, size_t buffer_size, size_t max_text_legth){
	shm_accessor_t *accessor;
	accessor = malloc(sizeof(shm_accessor_t));
	if (accessor == NULL)
	{
		return NULL;
	}
	strncpy(accessor->filename, shm_filename, PATH_MAX);
	accessor->fd = shm_open(shm_filename, SHMMEM_FLAGS, mode);
	accessor->fd_size = sizeof(shm_buffer_t) + buffer_size*max_text_legth;
	ftruncate(accessor->fd, accessor->fd_size);
	accessor->buffer_struct = mmap(NULL, accessor->fd_size, SHMMEM_PROT, MAP_SHARED, accessor->fd, 0);
	if (accessor->buffer_struct == (void *) -1)
	{
		printf("Heyyo\n");
		return 0;
	}

	accessor->buffer_struct->buffer_size = buffer_size;
	accessor->buffer_struct->max_text_legth = max_text_legth;
	accessor->buffer_struct->write_index = 0;
	accessor->buffer_struct->read_index = 0;
	accessor->buffer_struct->view = 0;
	accessor->buffer_struct->counter = 0;
	return accessor;
}

shm_ptr open_shm(const char *shm_filename, mode_t mode){
	shm_accessor_t *accessor;
	struct stat shm_file;
	accessor = malloc(sizeof(shm_accessor_t));
	//printf("%d\n", 14);
	if (accessor == NULL)
	{
		return NULL;
	}
	strncpy(accessor->filename, shm_filename, PATH_MAX);
	accessor->fd = shm_open(shm_filename, SHMMEM_FLAGS, mode);
	fstat(accessor->fd, &shm_file);
	accessor->fd_size = shm_file.st_size;
	accessor->buffer_struct = mmap(NULL, accessor->fd_size, SHMMEM_PROT, MAP_SHARED, accessor->fd, 0);
	accessor->buffer_struct->view = getpid();

	return accessor;
}

int write_shm(shm_ptr accessor, const char *text){
	if (accessor->buffer_struct->write_index >= accessor->buffer_struct->buffer_size)
	{
		return 0;
	}
	char * buffer_pointer = (char*) BUFFER_POINTER(accessor);
	strncpy(&buffer_pointer[(accessor->buffer_struct->write_index)*(accessor->buffer_struct->max_text_legth)], text, accessor->buffer_struct->max_text_legth);
	(accessor->buffer_struct->write_index)++;
	(accessor->buffer_struct->counter)++;
	return 1;
}

int read_shm(shm_ptr accessor, char *text){
	if (accessor->buffer_struct->read_index >= accessor->buffer_struct->buffer_size || accessor->buffer_struct->read_index >= accessor->buffer_struct->write_index || accessor->buffer_struct->counter <= 0)
	{
		*text = 0;
		return 0;
	}
	char * buffer_pointer = (char*) BUFFER_POINTER(accessor);
	strncpy(text, &buffer_pointer[(accessor->buffer_struct->read_index)*(accessor->buffer_struct->max_text_legth)],
		accessor->buffer_struct->max_text_legth);
	(accessor->buffer_struct->read_index)++;
	(accessor->buffer_struct->counter)--;
	return 1;
}

pid_t get_view_pid(shm_ptr accessor){
	return accessor->buffer_struct->view;
}

void close_shm(shm_ptr accessor){
	munmap((char *)BUFFER_POINTER(accessor), accessor->fd_size);
	close(accessor->fd);
	free(accessor);
}

void finalize_shm(shm_ptr accessor){
	munmap((char *)BUFFER_POINTER(accessor), accessor->fd_size);
	ftruncate(accessor->fd, 0);
	close(accessor->fd);
	shm_unlink(accessor->filename);
	free(accessor);
}