#ifndef _PIPES_H
#define _PIPES_H

/*** Defines ***/

#define PIPE_TERMINALS 2
#define READ_TERMINAL 0
#define WRITE_TERMINAL 1

/*** endDefines ***/

/*** TypeDeclarations ***/

typedef int pipe_t[PIPE_TERMINALS];

/*** endTypeDeclarations ***/

/*** Prototypes ***/

/*
	Sets the read terminal of the pipe (pipe[0]) as the STDIN.
	Anything that is usually read from STDIN will be read from
	pipe.
*/

void read_on_pipe(pipe_t pipe);

/*
	Sets the write terminal of the pipe (pipe[1]) as the STDOUT.
	Anything that is usually written to STDOUT will be written to
	pipe.
*/

void write_on_pipe(pipe_t pipe);

/*
	Sets a the specified terminal of the pipe as a replacement for
	the FileDescriptor give. Terminal should be either
	* READ_TERMINAL
	* WRITE_TERMINAL
*/

void listener_on_pipe(pipe_t pipe, int terminal, int fd);

void close_pipe(pipe_t pipe);

/*** endPrototypes ***/

#endif
