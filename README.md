#tp1-so

##IMPORTANT NOTE
Always ***pull*** before ***push***

##Formating in .md files
* Using an asterisk before a sentence creates a bullet point
* Encasing words in one asterisk generates *italics*
* Encasing words in two asterisks generates **bold**
* You can also ~~cross out~~ words

##Needs Fixin'
List every problem you encounter. For every problem create a *tabbed* bullet saying how it was solved.

* All variable names in *snake_case*
* ~~Program stays *waiting*~~

Both ./app /\* and slave are in state S meaning *interruptible sleep (waiting for an event to complete)*

```c
while((linelen = getline(&line, &linecap, stdin)) > 0)
	{
		linecount++;
		printf("Line %d: ", linecount);
    	fwrite(line, linelen, 1, stdout);
	}
```

###Actual Problem

After reading all lines the getline method keeps waiting for a \n to finish which never comes

###Solution

```c
while((linelen = getline(&line, &linecap, stdin)) > 0 && *line != '\n')
	{
		linecount++;
		printf("Line %d: ", linecount);
    	fwrite(line, linelen, 1, stdout);
	}
```

Checking that the first character in the line is not *'\n'*

##How did we think of that
* Comunicacion maestro-esclavo

Se comenzo por un simple programa para testear la comunicacion maestro-esclavo/esclavo-maestro 
que ademas permitio asentar las bases de la libreria de comunicacion entre pipes "pipes".
Al comenzar dicho programa solo comunicaba a un proceso esclavo con el padre y el codigo era 
el siguiente.

```c
//m.c
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include "pipes.h"

int main(void){
	char *buff = NULL;
	pipe_t up_pipe, down_pipe;
	size_t n = 0;
	pipe(up_pipe);
	pipe(down_pipe);
	if (fork() == 0)
	{
		read_on_pipe(down_pipe);
		write_on_pipe(up_pipe);
		execl("s", "s", NULL);
	}
	read_on_pipe(up_pipe);
	getline(&buff, &n, stdin);
	printf("%s\n", buff);
	return 0;
}
```
```c
//s.c
#include <stdio.h>

int main(void){
	printf("Arigato\n");
	return 0;
}
```

Como se puede observar, la idea central era utilizar dos pipes uno en el sentido padre-hijo
y otro en el inverso (hijo-padre).
Se comprobo el correcto funcionamiento que inmediantamente llevo a crear mas esclavos para 
observar su comportamiento a la hora de estar enviando simultaneamente informacion.

```c
//Cambios a m.c
#define SLAVE_COUNT 1000

int main(void){
	int i, pid; 
	char *buff = NULL;
	pipe_t up_pipe, down_pipe;
	size_t n = 0;
	pipe(up_pipe);
	pipe(down_pipe);
	for (i = 0; i < SLAVE_COUNT; ++i)
	{
		if ((pid = fork()) == 0)
		{
			read_on_pipe(down_pipe);
			write_on_pipe(up_pipe);
			execl("s", "s", NULL);
		}
		printf("%d\n", pid);
	}
	read_on_pipe(up_pipe);
	while(getline(&buff, &n, stdin) > 0){
		printf("%s\n", buff);
	}
	return 0;
}
```

```c
//Cambios a s.c
#define SLAVE_COUNT 1000

int main(void){
	int i;
	for (i = 0; i < MSGS; ++i)
	{
		printf("Arigato %d %d\n", getpid(), i);
	}	
	return 0;
}
```

La modificacion presentada arriba permitio observar que ocurria con la escritura
cuando los hijos deseaban escribir mucho y eran muchos. Se puede considerar
un stress test de escritura por el canal de comunicacion hijo padre.
Los resultados obtenidos fueron de interes

![Process](img/pipes1.png)
![Process](img/pipes2.png)

Mediante el siguiente sistema se hipotetizo que los writes son atomicos hasta
4096bytes (equivalente a 4096 caracteres) y mediante la *man page* ***man 7 pipe***
se puede comprobar que dicho numero esta incluido en la constante PIPE_BUF que
debe ser la reguladora de escritura sobre pipes.

```c
//Cambios a m.c
#define SLAVE_COUNT 2

int main(void){
	int i, pid; 
	char *buff = NULL;
	pipe_t up_pipe, down_pipe;
	size_t n = 0;
	pipe(up_pipe);
	pipe(down_pipe);
	for (i = 0; i < SLAVE_COUNT; ++i)
	{
		if ((pid = fork()) == 0)
		{
			read_on_pipe(down_pipe);
			write_on_pipe(up_pipe);
			execl("s_p_c", "s_p_c", NULL);
		}
		printf("%d\n", pid);
	}
	read_on_pipe(up_pipe);
	while(getline(&buff, &n, stdin) > 0){
		printf("%s\n", buff);
	}
	return 0;
}
``` 

```c
//s_p_c.c
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define MSGS 1000000

int main(void){
	int i, pid;
	pid = getpid();
	for (i = 0; i < MSGS; ++i)
	{
		putchar('a' + pid%20);
	}	
	return 0;
}
```

```bash
./m > "output.txt"
```

##Miscellaneous
* A  pipe  has  a limited capacity.  If the pipe is full, then a write(2)
will block or fail, depending on whether the  O_NONBLOCK  flag  is  set
(see  below).   Different implementations have different limits for the
pipe capacity.  Applications should not rely on a particular  capacity:
an  application  should  be designed so that a reading process consumes
data as soon as it is available, so that a  writing  process  does  not
remain blocked.
In Linux versions before 2.6.11, the capacity of a pipe was the same as
the system page size (e.g., 4096 bytes on i386).  Since  Linux  2.6.11,
the pipe capacity is 65536 bytes.  Since Linux 2.6.35, the default pipe
capacity is 65536 bytes, but the capacity can be queried and set  using
the  fcntl(2)  F_GETPIPE_SZ  and F_SETPIPE_SZ operations.  See fcntl(2)
for more information. ***man 7 pipe***
* POSIX.1-2001 says that write(2)s of less than PIPE_BUF  bytes  must  be
atomic:  the  output  data  is  written  to  the  pipe  as a contiguous
sequence.  Writes of more than PIPE_BUF bytes  may  be  nonatomic:  the
kernel  may  interleave  the data with data written by other processes.
POSIX.1-2001 requires PIPE_BUF to be at least 512  bytes.   (On  Linux,
PIPE_BUF  is  4096 bytes.)  The precise semantics depend on whether the
file descriptor is nonblocking (O_NONBLOCK), whether there are multiple
writers to the pipe, and on n, the number of bytes to be written. ***man 7 pipe***

